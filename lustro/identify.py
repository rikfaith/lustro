#!/usr/bin/env python3
# identify.py -*-python-*-

import faulthandler
import os
import shutil
import stat
import tempfile
import zipfile

import unrardll

import lustro.adhoc
import lustro.epub
import lustro.exif
import lustro.mediainfo
import lustro.mobi
import lustro.pdf


class Identify():
    def __init__(self, path, direntry=None, content=False, ocr='yes',
                 tmpdir='/tmp', prefix='lustro', debug=False):
        self.path = path
        self.direntry = direntry
        self.content = content
        self.ocr = ocr
        self.tmpdir = tmpdir
        self.prefix = prefix
        self.debug = debug
        self.tmpdirname = None

        faulthandler.enable()

        if self.direntry is None:
            try:
                self.stat = os.stat(self.path)
            except FileNotFoundError:
                self.stat = None
        else:
            try:
                self.stat = self.direntry.stat
            except FileNotFoundError:
                self.stat = None

    def __del__(self):
        if self.tmpdirname is not None:
            shutil.rmtree(self.tmpdirname)

    def _search_archives(self, files):
        self.tmpdirname = tempfile.mkdtemp(prefix=self.prefix, dir=self.tmpdir)
        for file in files:
            if file.endswith('.zip'):
                try:
                    zip = zipfile.ZipFile(file)
                    zip.extractall(self.tmpdirname)
                except Exception as exception:
                    return None, f'Error on {file}: {repr(exception)}'

        error = None
        for entry in os.scandir(self.tmpdirname):
            if entry.path.endswith('.rar'):
                try:
                    unrardll.extract(entry.path, self.tmpdirname)
                    error = None
                    break
                except Exception as exception:
                    # Save the error, but keep trying the other files because
                    # the unrardll cannot find the header itself (i.e., unlike
                    # unrar(1)).
                    error = f'Error on {entry.path}: {repr(exception)}'

        for entry in os.scandir(self.tmpdirname):
            if entry.path.endswith('.epub') or \
               entry.path.endswith('.mobi') or \
               entry.path.endswith('.pdf'):
                return entry.path, error

        return None, error

    def _analyze_directory(self):
        result = {}
        dirs = []
        files = []
        found_zip = False
        try:
            for entry in os.scandir(self.path):
                if entry.is_dir(follow_symlinks=False):
                    dirs.append(entry.path)
                elif entry.is_file(follow_symlinks=False):
                    files.append(entry.path)
                    if entry.path.endswith('.zip'):
                        found_zip = True
        except PermissionError:
            pass
        result['dirs'] = dirs
        result['files'] = files

        if found_zip and len(dirs) == 0:
            # This may be a special distribution.
            archived_file, error = self._search_archives(files)
            if error is not None:
                archived_file = None
                result['error'] = error
            if archived_file is not None:
                result['archived_file'] = archived_file

        return result

    def identify(self):
        result = {}

        result['path'] = self.path
        result['basename'] = os.path.basename(self.path)
        result['ext'] = os.path.splitext(self.path)[1]

        if self.stat is None:
            result['error'] = f'Error on {self.path}: cannot stat() file'
            return result

        result['mtime'] = self.stat.st_mtime
        result['bytes'] = self.stat.st_size

        if stat.S_ISDIR(self.stat.st_mode):
            result.update(self._analyze_directory())
            if 'archived_file' not in result:
                result['type'] = 'directory'
                return result
            result['type'] = 'archived_file'

        if stat.S_ISLNK(self.stat.st_mode):
            result['type'] = 'link'
            return result

        if 'type' not in result and not stat.S_ISREG(self.stat.st_mode):
            result['type'] = 'unknown'
            return result

        result['type'] = 'file'

        if 'archived_file' in result:
            working_path = result['archived_file']
        else:
            working_path = self.path

        result.update(lustro.adhoc.Adhoc.read(os.path.dirname(working_path),
                                              result.get('files', None),
                                              debug=self.debug))

        result.update(lustro.mediainfo.MediaInfo.read(working_path,
                                                      debug=self.debug))
        result.update(lustro.exif.Exif.read(working_path, debug=self.debug))

        match result.get('format', ''):
            case 'PDF':
                result.update(lustro.pdf.PDF.read(working_path,
                                                  content=self.content,
                                                  ocr=self.ocr,
                                                  debug=self.debug))
            case 'EPUB':
                result.update(lustro.epub.EPub.read(working_path,
                                                    content=self.content,
                                                    debug=self.debug))
            case 'MOBI':
                result.update(lustro.mobi.Mobi.read(working_path,
                                                    content=self.content,
                                                    debug=self.debug))

        return result
