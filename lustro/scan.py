#!/usr/bin/env python3
# scan.py -*-python-*-

# We use multiprocessing.Queue, but we need queue.Empty from queue.
import concurrent.futures
import faulthandler
import gc
import json
import multiprocessing
import os
import queue
import resource
import signal
import stat
import sys
import threading
import time

import psutil

import lustro.identify
import lustro.format

# pylint: disable=unused-import
from lustro.log import DEBUG, INFO, ERROR, FATAL, DECODE


class Scan():
    IDLE_TIME = 5

    def __init__(self, directories, json_dir=None, content=False, ocr='yes',
                 max_workers=3, max_kilobytes=0, debug=False):
        self.directories = directories
        self.json_dir = json_dir
        self.content = content
        self.ocr = ocr
        self.max_workers = max_workers + 1
        self.max_kilobytes = max_kilobytes
        self.debug = debug

        self.fatal = False
        self.executor = None
        self.restart = [(True, 0)] * self.max_workers
        self.last_message = [None] * self.max_workers

        # Reserve at least 1GB for each non-idle worker.
        if self.max_kilobytes < 1024*1024:
            system_kilobytes = psutil.virtual_memory().total / 1024
            self.max_kilobytes = system_kilobytes / max_workers / 2
        if self.max_kilobytes < 1024*1024 and max_workers != 1:
            FATAL(f'Not enough memory for {max_workers} workers:'
                  f' {self.max_kilobytes:.0f}KiB')
        DEBUG(f'{self.max_kilobytes=}')

        if sys.version_info[0] != 3 or sys.version_info[1] < 11:
            FATAL('Scanning requires Python 3.11 or later')

    @staticmethod
    def _log_callback(idx, msg_type, payload):
        DEBUG(f'worker {idx}: {msg_type} {payload}')

    @staticmethod
    def _is_document_or_directory(entry):
        try:
            is_file = entry.is_file()
            is_dir = entry.is_dir()
        except OSError:
            # This is often an error regarding too
            # many symbolic links.
            return False

        if is_dir:
            return True

        if not is_file:
            return False

        _, ext = os.path.splitext(entry.name)
        if ext.lower()[1:] not in [
                'azw',
                'azw3',
                'cb7',
                'cba',
                'cbr',
                'cbt',
                'cbz',
                'ceb',
                'chm',
                'djvu',
                'doc',
                'docx',
                'epub',
                'fb2',
                'htm',
                'html',
                'iba',
                'ibooks',
                'inf',
                'kf8',
                'kfx',
                'lit',
                'lrf',
                'lrs',
                'lrx',
                'mobi',
                'opf',
                'oxps',
                'pdb',
                'pdf',
                'pdg',
                'prc',
                'ps',
                'rft',
                'rtf',
                'tr2',
                'tr3',
                'txt',
                'xeb',
                'xml',
                'xps',
                ]:
            return False
        return True

    @staticmethod
    def _worker(idx, unique, workq, resultq, json_dir, content, ocr,
                documents_only, parent_pid, max_kilobytes):

        def internal_worker(idx, unique, workq, resultq, max_kilobytes):
            working = False
            memory_check_time = time.time()
            while True:
                if time.time() - memory_check_time > 5:
                    memory_check_time = time.time()
                    rusage = resource.getrusage(resource.RUSAGE_SELF)
                    resultq.put((idx, 'memory', rusage.ru_maxrss))
                    if rusage.ru_maxrss > max_kilobytes:
                        return 'restart', unique

                try:
                    command, basename, dirname = workq.get(True, 1)
                except queue.Empty:
                    if working:
                        resultq.put((idx, 'idle', None))
                    time.sleep(Scan.IDLE_TIME)
                    continue

                if command == 'quit':
                    return 'quit', unique

                if command != 'entry':
                    resultq.put((idx, 'error',
                                 'command={} basename={} dirname={}'.format(
                                     command, basename, dirname)))
                    return 'unknown command', unique

                if not working:
                    resultq.put((idx, 'working', None))
                working = True

                if basename is not None:
                    fulldirname = os.path.join(basename, dirname)
                else:
                    fulldirname = dirname

                resultq.put((idx, 'message', fulldirname))
                identify = lustro.identify.Identify(fulldirname, ocr=ocr,
                                                    content=content)
                ident = identify.identify()

                if 'error' in ident:
                    resultq.put((idx, 'error', ident['error']))
                    ident.pop('error', None)

                if json_dir is not None and \
                   ident.get('type', '') == 'file' and \
                   ident['bytes'] != 0:

                    ident['json'] = f'{idx}-{unique}.json'
                    outfile = os.path.join(json_dir, ident['json'])
                    unique += 1
                    try:
                        with open(outfile, 'w') as fp:
                            fp.write(json.dumps(ident, indent=4,
                                                sort_keys=False))
                    except PermissionError:
                        resultq.put((idx, 'error', f'Cannot open {outfile}'))
                        return 'permission error', unique

                # Get rid of the content before sending the info over the wire.
                ident.pop('content', None)
                resultq.put((idx, 'id', ident))

                if ident.get('type', '') == 'directory':
                    try:
                        with os.scandir(fulldirname) as entries:
                            for entry in entries:
                                if entry.name in ['.', '..']:
                                    continue
                                if documents_only and \
                                   not Scan._is_document_or_directory(entry):
                                    continue
                                workq.put(('entry', fulldirname, entry.name))
                    except PermissionError:
                        pass

        def terminator():
            while True:
                if stop_thread:
                    return
                try:
                    os.kill(parent_pid, 0)
                except OSError:
                    FATAL(f'Terminator for {idx=} killing {parent_pid=}')
                    os.kill(os.getpid(), signal.SIGTERM)
                time.sleep(1)

        assert workq
        assert resultq

        # get stack backtrace for 3rd party libraries.
        faulthandler.enable()

        stop_thread = False
        thread = threading.Thread(target=terminator, daemon=True)
        thread.start()
        resultq.put((idx, 'pid', os.getpid()))

        result = 'normal exit', -1
        try:
            if idx == 0:
                # This is the idle thread which keeps the ProcessPoolExecutor
                # from shutting down.
                while True:
                    time.sleep(Scan.IDLE_TIME)
            else:
                result = internal_worker(idx, unique, workq, resultq,
                                         max_kilobytes)
        except KeyboardInterrupt:
            result = 'keyboard interrupt', -1

        stop_thread = True
        thread.join()
        return result

    def _done_callback(self, idx, future):
        ERROR(f'worker {idx}: {self.last_message[idx]}')

        # Terminate all processes if there was an exception.
        exc = future.exception()
        if exc is not None:
            ERROR(f'worker {idx}: {str(exc)}')
            self.fatal = True
            return

        try:
            result = future.result()
        except concurrent.futures.process.BrokenProcessPool:
            ERROR(f'worker {idx}: process pool is broken')
            self.fatal = True
            return

        if result[0] == 'restart':
            DEBUG(f'worker {idx}: {result}')
            self.restart[idx] = (True, result[1])
        elif result is not None:
            ERROR(f'worker {idx}: {result}')
            self.fatal = True

    def _terminate(self, message, pids, workq=None):
        ERROR(message)
        if workq is not None:
            for idx in range(self.max_workers):
                workq.put(('quit', None, None))

        sleep_time = Scan.IDLE_TIME + 2
        INFO(f'Sleeping for {sleep_time} seconds')
        time.sleep(sleep_time)

        for idx in range(self.max_workers):
            pid = pids[idx]
            if pid == 0:
                continue
            try:
                DEBUG(f'Killing {idx}: {pid=}')
                os.kill(pid, signal.SIGTERM)
            except OSError:
                pass
        self.executor.shutdown(wait=False)
        DEBUG('Shutdown complete')

    def scan(self, progress=False, callback=_log_callback,
             documents_only=False):
        # Start the workers
        manager = multiprocessing.Manager()
        workq = manager.Queue()
        resultq = manager.Queue()
        resultq.put((-1, 'test', None))
        DEBUG('Starting {} concurrent worker(s)'.format(self.max_workers))
        self.executor = concurrent.futures.ProcessPoolExecutor(
            max_workers=self.max_workers,
            max_tasks_per_child=1)

        try:
            # Fill the queue.
            for directory in self.directories:
                if directory[0] == '/':
                    DEBUG('Adding {}'.format(directory))
                    workq.put(('entry', None, directory))
                else:
                    DEBUG('Adding {} in {}'.format(directory, os.getcwd()))
                    workq.put(('entry', os.getcwd(), directory))

            # Main loop.
            messages = 0
            identities = 0
            futures = [None] * self.max_workers
            working = [False] * self.max_workers
            pids = [0] * self.max_workers
            memory = [0] * self.max_workers
            last_message = [0] * self.max_workers
            summary = {}
            summary_time = 0
            restarts = 0
            while True:
                # Check for terminated workers.
                if self.fatal:
                    DEBUG(f'Detected failed worker via callback')
                    self._terminate('Terminating because of failed worker',
                                    pids, workq)
                    return

                # Check for restartable workers.
                for idx, (restart, unique) in enumerate(self.restart):
                    if restart == True:
                        self.restart[idx] = (False, unique)
                        DEBUG(f'Restarting worker {idx}')
                        restarts += 1
                        assert unique >= 0
                        future = self.executor.submit(self._worker, idx,
                                                      unique, workq, resultq,
                                                      self.json_dir,
                                                      self.content, self.ocr,
                                                      documents_only,
                                                      os.getpid(),
                                                      self.max_kilobytes)
                        futures[idx] = future
                        future.add_done_callback(
                            lambda future, idx=idx: self._done_callback(
                                idx, future))

                # Check for other failures. XXX FIXME is this needed?
                found_done_future = False
                for future in futures:
                    if future.done():
                        found_done_future = True
                if found_done_future:
                    DEBUG(f'Detected done future')
                    self._terminate('Terminating because of done future',
                                    pids, workq)
                    return

                # Get next message.
                try:
                    message = resultq.get(True, 1)
                except queue.Empty:
                    # We've completed all the available work.
                    if sum(working) == 0 and workq.qsize() == 0 and \
                       resultq.qsize() == 0:
                        break
                    continue

                messages += 1
                now = time.time()
                last_message[message[0]] = now
                if progress and time.time() - summary_time > 1.5:
                    summary_time = time.time()
                    if len(summary) > 0:
                        INFO(f'Memory (MB): {[int(m/1024) for m in memory]};'
                             f' {restarts=}')
                        INFO('Lastmsg (s):'
                             f' {[int(now - v) for v in last_message]}')
                        INFO('workq=%d resultq=%d workers=%d msgs=%d ids=%d',
                              workq.qsize(), resultq.qsize(), sum(working),
                              messages, identities)
                        INFO(f'Summary: {sum(summary.values())} {summary}')

                # Process message.
                if message[1] == 'pid':
                    pids[message[0]] = int(message[2])

                if message[1] == 'memory':
                    memory[message[0]] = int(message[2])

                if message[1] == 'message':
                    INFO('worker %d: %s', message[0], message[2])
                    self.last_message[message[0]] = message[2]

                if message[1] == 'working':
                    working[message[0]] = True

                if message[1] == 'idle':
                    working[message[0]] = False

                if message[1] == 'error':
                    ERROR('worker %d: %s', message[0], message[2])
                    if 'error' in summary:
                        summary['error'] += 1
                    else:
                        summary['error'] = 1
                if message[1] == 'id':
                    identities += 1
                    kind = message[2].get('format', None)
                    if kind is None:
                        kind = message[2].get('type', None)
                    if kind:
                        if kind in summary:
                            summary[kind] += 1
                        else:
                            summary[kind] = 1

                # Call callback.
                callback(*message)
        except KeyboardInterrupt:
            self._terminate('Terminating because of Control-C', pids)
            return

        self._terminate('Terminating because work completed', pids, workq)
