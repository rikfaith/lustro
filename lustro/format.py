#!/usr/bin/env python3
# format.py -*-python-*-

import json
import time

import humanize

# pylint: disable=unused-import
from lustro.log import LOG_SET_LEVEL, DEBUG, INFO, ERROR, FATAL, DECODE


class Format():
    @staticmethod
    def _nonempty_string(metadata, field, prefix='\n    '):
        value = str(metadata.get(field, ''))
        if len(value) < 1:
            return ''
        return f'{prefix}{value}'

    @staticmethod
    def pretty_print(metadata, full=False):
        result = metadata.get('path', '') + '\n'
        if 'archived_file' in metadata:
            result += f'    Archived file: {metadata["archived_file"]}\n'


        if 'bytes' in metadata:
            size = metadata['bytes']
            hsize = humanize.naturalsize(size, binary=True)
            result += f'    {size} ({hsize})'

        if 'mtime' in metadata:
            timestamp = time.strftime('%Y %b %d %H:%M',
                                      time.localtime(metadata['mtime']))
            result += f' {timestamp}'

        fmt = metadata.get('format', '')
        if 'format' in metadata:
            result += Format._nonempty_string(metadata, fmt, prefix=' ')

        if metadata.get('type', '') == 'directory':
            files = len(metadata['files'])
            dirs = len(metadata['dirs'])
            result += f' [directory: {files} files, {dirs} subdirectories]'

        if fmt == 'PDF':
            pdf_version = metadata.get('pdf_version', None)
            if pdf_version is not None:
                result += f' (PDF {pdf_version})'

        pages = metadata.get('pages', None)
        if pages is not None:
            result += f' ({pages}p)'

        width = metadata.get('width', None)
        height = metadata.get('height', None)
        if width is not None and height is not None:
            result += f' ({width}x{height})'

        result += Format._nonempty_string(metadata, 'title')
        result += Format._nonempty_string(metadata, 'author')
        result += Format._nonempty_string(metadata, 'publisher')
        result += Format._nonempty_string(metadata, 'isbn')
        result += Format._nonempty_string(metadata, 'error')

        if full:
            result += '\n'
            formatted = json.dumps(metadata, indent=4, sort_keys=False)
            padded = '    '.join(formatted.splitlines(True))
            result += f'    {padded}'

        return result
