#!/usr/bin/env python3
# mediainfo.py -*-python-*-

import json
import re

import pymediainfo

# pylint: disable=unused-import
from lustro.log import LOG_SET_LEVEL, DEBUG, INFO, ERROR, FATAL, DECODE


class MediaInfo():
    @staticmethod
    def _add(result, field, data, append=False):
        if data is not None and data != '':
            if isinstance(data, str) and len(data) > 20:
                data = re.sub(' /.*', '', data)
            if field not in result:
                result[field] = data
            elif append:
                result[field] = result[field] + ',' + data

    @staticmethod
    def _extract_general(result, track):
        MediaInfo._add(result, 'encoded_date', track.encoded_date)
        MediaInfo._add(result, 'tagged_date', track.tagged_date)
        MediaInfo._add(result, 'format', track.format)
        MediaInfo._add(result, 'rating', track.rating)
        MediaInfo._add(result, 'duration', track.duration)
        MediaInfo._add(result, 'performer', track.performer)
        MediaInfo._add(result, 'album', track.album)
        MediaInfo._add(result, 'track', track.track_name)

    @staticmethod
    def _extract_video(result, track):
        MediaInfo._add(result, 'video_codec', track.codec_id)
        MediaInfo._add(result, 'video_codec', track.commercial_name)
        MediaInfo._add(result, 'width', track.width)
        MediaInfo._add(result, 'height', track.height)

    @staticmethod
    def _extract_audio(result, track):
        MediaInfo._add(result, 'audio_codec', track.codec_id)
        MediaInfo._add(result, 'audio_codec', track.commercial_name)
        MediaInfo._add(result, 'audio_mode', track.bit_rate_mode)
        MediaInfo._add(result, 'audio_rate', track.bit_rate)

    @staticmethod
    def _extract_image(result, track):
        MediaInfo._add(result, 'width', track.width)
        MediaInfo._add(result, 'height', track.height)

    @staticmethod
    def _extract_text(result, track):
        MediaInfo._add(result, 'language', track.language, append=True)

    @staticmethod
    def read(path, debug=False):
        result = {}
        try:
            info = pymediainfo.MediaInfo.parse(path, cover_data=True)
        except RuntimeError:
            return result
        except UnicodeEncodeError:
            return result

        if debug:
            print('MEDIAINFO METADATA',
                  json.dumps(json.loads(info.to_json()), indent=4,
                             sort_keys=False), flush=True)

        for track in info.tracks:
            if track.track_type == 'General':
                MediaInfo._extract_general(result, track)
            if track.track_type == 'Video':
                MediaInfo._extract_video(result, track)
            if track.track_type == 'Audio':
                MediaInfo._extract_audio(result, track)
            if track.track_type == 'Image':
                MediaInfo._extract_image(result, track)
            if track.track_type == 'Text':
                MediaInfo._extract_text(result, track)

        return result
