#!/usr/bin/env python3
# mobi.py -*-python-*-

import datetime
import json
import logging
import os
import subprocess
import sys
import tempfile

import lustro.epub

# pylint: disable=unused-import
from lustro.log import LOG_SET_LEVEL, DEBUG, INFO, ERROR, FATAL, DECODE


class Mobi():
    def __init__(self, path, tmpdir='/tmp', prefix='lustro', debug=False):
        self.path = path
        self.tmpdir = tmpdir
        self.prefix = prefix
        self.debug = debug

        # First thing, convert this to an epub using mobitool(1). The library
        # support for mobi extraction is limited. Calibre has it, but Calibre
        # doesn't play well with Python 3.11.
        self.subtmpdir, self.epubfile = self.convert()
        if self.epubfile is None:
            self.epub = None
        else:
            self.epub = lustro.epub.EPub(self.epubfile, debug=self.debug)

    def convert(self):
        tmpdir = tempfile.TemporaryDirectory(prefix=self.prefix,
                                             dir=self.tmpdir,
                                             ignore_cleanup_errors=True)
        root, _ = os.path.splitext(self.path)
        outputfile = os.path.join(tmpdir.name,
                                  os.path.basename(root) + '.epub')
        DEBUG(f'Using temporary file {outputfile} for mobitool')
        with subprocess.Popen(['mobitool',
                               '-e',
                               '-o',
                               tmpdir.name,
                               self.path],
                              stdout=subprocess.PIPE,
                              stderr=subprocess.PIPE) as proc:
            results = proc.communicate()[0]
            proc.wait()
            for result in results.split(b'\n'):
                DEBUG(f'mobitool: {result.decode("utf-8")}')
            if proc.returncode != 0:
                return None, None
        return tmpdir, outputfile

    def get_metadata(self):
        if self.epub is None:
            return {}
        result = self.epub.read_opf()
        return result

    def _extract_text(self):
        if self.epub is None:
            return {}, None
        return self.epub._extract_text()

    @staticmethod
    def read(path, content=False, debug=False):
        mobi = Mobi(path, debug=debug)
        result = mobi.get_metadata()
        if debug:
            print('MOBI METADATA', json.dumps(result, indent=4,
                                              sort_keys=False), flush=True)
        if content:
            try:
                cnt, isbn = mobi._extract_text()
            except Exception as exception:
                result['error'] = f'Error on {path}: {repr(exception)}'
                return result
            if isbn is not None:
                result['isbn'] = isbn
            if len(cnt) > 0:
                result['content'] = cnt
        return result
