#!/usr/bin/env python3
# adhoc.py -*-python-*-

import os
import re


class Adhoc():
    @staticmethod
    def _process(path, debug=False):
        result = {}

        if not os.path.exists(path):
            return result

        with open(path, 'r', encoding='utf-8', errors='replace') as fp:
            for line in fp.readlines():
                if re.search(r'ISBN', line):
                    isbn = re.sub(r'.*ISBN', '', line)
                    isbn = re.sub(r'^[^0-9]*', '', isbn)
                    isbn = re.sub(r'[^0-9]*$', '', isbn)
                    result['isbn'] = isbn.strip()

        return result

    @staticmethod
    def read(directory, files=None, debug=False):
        result = {}

        if files is None:
            files = []
            for entry in os.scandir(directory):
                if entry.is_file(follow_symlinks=False):
                    files.append(entry.path)

        for file in files:
            if file.endswith('.nfo'):
                result.update(Adhoc._process(os.path.join(directory, file)))

        return result
