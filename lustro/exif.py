#!/usr/bin/env python3
# exif.py -*-python-*-

import json
import re
import subprocess


class Exif():
    @staticmethod
    def _add(result, field, data, force=False):
        if data is not None and data != '':
            if isinstance(data, str) and len(data) > 20:
                data = re.sub(' /.*', '', data)
            if force or field not in result:
                result[field] = data

    @staticmethod
    def read(path, debug=False):
        result = {}
        proc = subprocess.run(['exiftool', '-c', '%f', '-j', path],
                              capture_output=True,
                              text=True,
                              check=False)
        if proc.returncode != 0:
            return result

        exif = json.loads(proc.stdout)[0]

        if debug:
            print('EXIF METADATA', json.dumps(exif, indent=4, sort_keys=False),
                  flush=True)

        # For PDFs
        Exif._add(result, 'pdf_version', exif.get('PDFVersion', None))
        Exif._add(result, 'pages', exif.get('PageCount', None))
        Exif._add(result, 'title', exif.get('Title', None))
        Exif._add(result, 'author', exif.get('Author', None))
        Exif._add(result, 'subject', exif.get('Subject', None))
        Exif._add(result, 'publisher', exif.get('Publisher', None))
        Exif._add(result, 'description', exif.get('Description', None))
        Exif._add(result, 'create_date', exif.get('CreateDate', None))
        Exif._add(result, 'isbn', exif.get('ISBN', None))
        Exif._add(result, 'format', exif.get('FileType', None), force=True)

        # For images
        Exif._add(result, 'width', exif.get('ImageWidth', None), force=True)
        Exif._add(result, 'height', exif.get('ImageHeight', None), force=True)
        Exif._add(result, 'camera', exif.get('Model', None))
        Exif._add(result, 'lens', exif.get('LensModel', None))
        Exif._add(result, 'shutter', exif.get('ShutterSpeed', None))
        Exif._add(result, 'aperture', exif.get('Aperture', None))
        Exif._add(result, 'focal_length', exif.get('FocalLength', None))
        Exif._add(result, 'gps_date', exif.get('GPSDateTime', None))
        Exif._add(result, 'gps_lon', exif.get('GPSLongitude', None))
        Exif._add(result, 'gps_lat', exif.get('GPSLatitude', None))
        return result
