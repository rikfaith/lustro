#!/usr/bin/env python3
# epub.py -*-python-*-

import json
import re
import time
import zipfile

import fitz
import xmltodict


class EPub():
    def __init__(self, path, debug=False):
        self.path = path
        self.debug = debug

        fitz.TOOLS.mupdf_display_errors(False)
        fitz.TOOLS.store_shrink(100)
        try:
            self.document = fitz.Document(self.path)
        except:
            # This is probably because of an incorrect DRM error (EPUB is
            # locked by DRM). We can still get metadata, but not text unless
            # we unpack the .zip, remove the META-INF/rights.xml and
            # META-INF/encryption.xml, repack the .zip, and then see if mupdf
            # will open the new .zip. (See mupdf/source/html/epub-doc.c)
            self.document = None

    def __del__(self):
        if self.document is not None:
            self.document.close()

    @staticmethod
    def _clean(text):
        lines = []
        for line in text.split('\n'):
            line = line.strip()
            if len(line) == 0:
                continue
            lines.append(line)
        return ' '.join(lines)

    def _extract_text(self):
        isbn = None
        result = []
        if self.document is None:
            return result
        for pageno, page in enumerate(self.document.pages(0, 1, 1)):
            blocks = page.get_text('blocks', flags=fitz.TEXT_INHIBIT_SPACES)
            line_in_work = 0
            for _, _, _, _, text, block_no, block_type in blocks:
                if block_type == 1:
                    # Skip image blocks
                    continue

                line_in_work += 1
                line = {}
                line['page'] = pageno
                line['line'] = block_no
                line['line_in_work'] = line_in_work
                line['text'] = self._clean(text)
                assert line['text'] is not None
                if len(line['text']) > 0:
                    result.append(line)
                    if re.search('ISBN: ([-X0-9]+)', line['text']):
                        isbn = re.sub(r'.*ISBN: ([-X0-9]+).*', '\\1',
                                      line['text'])
        return result, isbn

    @staticmethod
    def add(result, field, data, force=False):
        if data is None or data == '':
            return

        if isinstance(data, dict):
            if '#text' in data:
                # Fix metadata that uses a dictionary.
                data = data['#text']
            else:
                # Skip the field for now. FIXME: improve this.
                return

        if isinstance(data, list) and isinstance(data[0], dict):
            clean = []
            for entry in data:
                if '#text' in entry:
                    clean.append(entry['#text'])
            data = clean

        # FIXME: Instead, convert some fields into lists.
        if isinstance(data, str) and len(data) > 20:
            data = re.sub(' /.*', '', data)

        if force or field not in result:
            result[field] = data

    def read_opf(self):
        result = {}
        try:
            zf = zipfile.ZipFile(self.path)
        except zipfile.BadZipFile:
            return result
        for zfile in zf.filelist:
            if zfile.filename.endswith('.opf'):
                try:
                    xml = zf.read(zfile.filename)
                except zipfile.BadZipFile:
                    continue
                try:
                    if isinstance(xml, bytes):
                        xml = xml.rstrip(b'\0')
                    opf = xmltodict.parse(xml)
                except Exception as exception:
                    result['error'] = f'Error on {zfile.filename} from' + \
                        f' {self.path}: {repr(exception)}'
                    return result
                if self.debug:
                    print(f'EPUB OPF ({zfile.filename})',
                          json.dumps(opf, indent=4, sort_keys=False),
                          flush=True)
                if 'package' not in opf:
                    continue
                if 'metadata' not in opf['package']:
                    continue
                metadata = opf['package']['metadata']
                EPub.add(result, 'title', metadata.get('dc:title', None))
                EPub.add(result, 'author', metadata.get('dc:author', None))
                EPub.add(result, 'subject', metadata.get('dc:subject', None))
                EPub.add(result, 'publisher', metadata.get('dc:publisher',
                                                           None))
                EPub.add(result, 'description', metadata.get('dc:description',
                                                             None))
                if 'dc:identifier' not in metadata:
                    continue
                for record in metadata['dc:identifier']:
                    if '@opf:scheme' in record and '#text' in record:
                        if record['@opf:scheme'] == 'ISBN':
                            EPub.add(result, 'isbn', record['#text'])
        return result

    @staticmethod
    def read(path, content=False, debug=False):
        epub = EPub(path, debug=debug)
        result = epub.read_opf()
        if debug:
            print('EPUB METADATA', json.dumps(epub.document.metadata, indent=4,
                                              sort_keys=False), flush=True)
        if epub.document is None:
            return result

        EPub.add(result, 'title',
                 epub.document.metadata.get('title', None),
                 force=True)
        EPub.add(result, 'author',
                 epub.document.metadata.get('author', None),
                 force=True)
        EPub.add(result, 'subject',
                 epub.document.metadata.get('subject', None),
                 force=True)
        EPub.add(result, 'keywords',
                 epub.document.metadata.get('keywords', None),
                 force=True)

        if content:
            try:
                cnt, isbn = epub._extract_text()
            except Exception as exception:
                result['error'] = f'Error on {path}: {repr(exception)}'
                return result
            if isbn is not None:
                result['isbn'] = isbn
            if len(cnt) > 0:
                result['content'] = cnt

        return result
