#!/usr/bin/env python3
# log.py -*-python-*-
'''
This is a simple addition to the standard Python logging infrastructure
that formats log messages with a date stamp and some additional information
that is helpful for debugging.

LICENSE
  This is free and unencumbered software released into the public domain.

  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.

  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

  For more information, please refer to <http://unlicense.org/>
'''

import inspect
import logging
import os
import sys
import time
import traceback


class Log():
    '''
    This file should be imported into modules that need logging:

    from log import DEBUG, INFO, ERROR, FATAL, DECODE

    or, if setting the level:

    from log import LOG_SET_LEVEL, DEBUG, INFO, ERROR, FATAL, DECODE

    Then, in your code, log a message:

    value = 42
    INFO('This in an informational message, value=%d', value)
    '''

    logger = None
    initial_level_set = False

    class LogFormatter(logging.Formatter):
        '''
        This class is used as a formatter for the logging infrastructure.
        '''
        def __init__(self):
            logging.Formatter.__init__(self)

        def format(self, record):
            # We pass a secret parameter called _depth from our internal
            # routines so that we can correct the stack offset.
            depth = record.__dict__.get('_depth', 0)

            level = record.levelname[0]
            date = time.localtime(record.created)
            date_msec = int((record.created - int(record.created)) * 1000)
            stamp = f'{level[0]}'
            stamp += f'{date.tm_year:04d}{date.tm_mon:02d}{date.tm_mday:02d}'
            stamp += f' {date.tm_hour:02d}:{date.tm_min:02d}'
            stamp += f':{date.tm_sec:02d}.{date_msec:03d}'

            caller = inspect.getframeinfo(inspect.stack()[9+depth][0])
            filename = '/'.join(caller.filename.split('/')[-2:])
            lineno = f'{filename}:{caller.lineno}'

            pid = f'{os.getpid()}'

            message = f'{stamp} {lineno} {pid} {Log.format_message(record)}'
            record.getMessage = lambda: message
            return logging.Formatter.format(self, record)

    class LogFilter(logging.Filter):
        def __init__(self):
            logging.Filter.__init__(self)

        def filter(self, record):
            target_level = logging.getLogger(record.module).getEffectiveLevel()
            if record.levelno < target_level:
                return False
            return True

    def __init__(self):
        '''
        Initialize the logging infrastructure and install our formatter.
        '''
        Log.logger = logging.getLogger()
        logging.addLevelName(50, 'FATAL')
        handler = logging.StreamHandler()
        handler.setFormatter(Log.LogFormatter())
        Log.logger.addHandler(handler)
        Log.logger.addFilter(Log.LogFilter())
        self.set_level('INFO')

    @staticmethod
    def format_message(record):
        '''
        Format the original (bare) log message and return it as a string.
        '''
        # pylint: disable=broad-except
        try:
            msg = record.msg % record.args
        except Exception as exception:
            msg = repr(record.msg) + \
                ' EXCEPTION: ' + repr(exception) + \
                ' record.msg=' + repr(record.msg) + \
                ' record.args=' + repr(record.args)
        return msg

    @staticmethod
    def set_level(level, module=None):
        '''
        Set the logging level. This is a global setting.
        '''
        if module is None:
            logger = Log.logger
        else:
            logger = logging.getLogger(module)

        old_level = logger.getEffectiveLevel()
        logger.setLevel(level)
        new_level = logger.getEffectiveLevel()

        if Log.initial_level_set:
            msg = ''
            if module is not None:
                msg = f' for module {module}'
            Log.logger.info('Log level change from %s (%s) to %s (%s)%s',
                            old_level, logging.getLevelName(old_level),
                            new_level, logging.getLevelName(new_level), msg)
            Log.initial_level_set = True
        return old_level

    @staticmethod
    def fatal(message, *args, **kwargs):
        '''
        Process a FATAL message. This will terminate the process.
        '''
        logging.fatal(message, *args, **kwargs, extra={'_depth': 1})
        sys.exit(1)

    @staticmethod
    def get_stack(skip=3):
        '''
        Helper function for providing a stack traceback. The skip value
        specifies how many items on the stack we will skip. If this is called
        by decode(), those items are get_stack(), decode(), and the line on
        which DECODE is called ( which is already reported by LogFormatter).
        '''

        msg = ''
        stack = traceback.extract_stack(limit=7)
        for filename, lineno, _, _ in stack[:-skip]:
            if msg != '':
                msg += '; '
            msg += f'{os.path.basename(filename)}:{lineno}'
        return msg

    @staticmethod
    def decode(message, *args, **kwargs):
        '''
        Decodes the most recent exception and then logs that information,
        along with a brief stack backtrace, at the ERORR level.
        '''
        stack = Log.get_stack()
        exc_type, exc_value, _ = sys.exc_info()
        if exc_type is None or exc_value is None:
            # No exception, just output the stack track information.
            logging.error(f'{message}: ({stack})', *args, **kwargs,
                          extra={'_depth': 1})
        else:
            exc = traceback.format_exception_only(exc_type, exc_value)
            logging.error(f'{message}: {exc[0].strip()} ({stack})',
                          *args, **kwargs, extra={'_depth': 1})


# Define global aliases to debugging functions.
DEBUG = logging.debug
INFO = logging.info
ERROR = logging.error
FATAL = Log.fatal
DECODE = Log.decode
LOG_SET_LEVEL = Log.set_level

# Instantiate the logging class
Log()
