#!/usr/bin/env python3
# main.py -*-python-*-

import argparse
import json
import os
import platform
import sys

import distro
import fitz

import lustro.format
import lustro.identify
import lustro.scan

# pylint: disable=unused-import
from lustro.log import LOG_SET_LEVEL, DEBUG, INFO, ERROR, FATAL, DECODE


def pretty_print(idx, msg_type, payload, full=False):
    if msg_type == 'error':
        ERROR(f'worker {idx}: {payload}')
    if msg_type in ['starting', 'stopping']:
        DEBUG(f'worker {idx}: {msg_type}')
    if msg_type != 'id':
        return

    if payload.get('type') == 'directory':
        return
    line = lustro.format.Format.pretty_print(payload, full=full)
    try:
        print(line)
    except UnicodeEncodeError as exception:
        DECODE('Formatting error')
        print(json.dumps(payload, indent=4, sort_keys=False), flush=True)
        # To terminate on this error, use os._exit(1) here, since sys.exit()
        # will hang.


def payload_logger(idx, msg_type, payload, full=False):
    if msg_type == 'error':
        ERROR(f'worker {idx}: {payload}')
    if msg_type in ['starting', 'stopping']:
        DEBUG(f'worker {idx}: {msg_type}')
    if msg_type != 'id':
        return

    path = payload.get('path', None)
    json = payload.get('json', None)
    fmt = payload.get('format', None)

    if json is None:
        if path.endswith('.epub') or path.endswith('.pdf'):
            ERROR(f'No JSON for {path}')
        return
    INFO('%s %s <- %s', fmt, json, os.path.basename(path))


def main():
    parser = argparse.ArgumentParser(description='lustro')
    parser.add_argument('paths', nargs='*',
                        help='List of directories and files to analyze')
    parser.add_argument('--json', default=None,
                        help='Directory in which to store JSON output'
                        ' (requires --recursive)')
    parser.add_argument('--content', action='store_true', default=False,
                        help='Extract content')
    parser.add_argument('--full', action='store_true', default=False,
                        help='Display all identification data')
    parser.add_argument('--version', action='store_true', default=False,
                        help='Display version and exit')
    parser.add_argument('--documents', action='store_true', default=False,
                        help='Scan documents only')
    parser.add_argument('--debug', action='store_true', default=False,
                        help='Increase logging verbosity')
    parser.add_argument('--nodebug', nargs='+', metavar='MODULE',
                        help='Modules for which debugging is disabled')
    parser.add_argument('--raw', action='store_true', default=False,
                        help='Display raw metadata for debugging')
    parser.add_argument('--recursive', action='store_true', default=False,
                        help='Identify all files in a directory hierarchy')
    parser.add_argument('--workers', default=1,
                        help='Number of concurrent workers for recursive scan')
    parser.add_argument('--progress', action='store_true', default=False,
                        help='Provide progress log messages during scan')
    parser.add_argument('--ocr', default='yes',
                        help='OCR: force=always, no=never, yes=if needed')

    args = parser.parse_args()

    if args.debug:
        LOG_SET_LEVEL('DEBUG')
        if args.nodebug:
            for module in args.nodebug:
                LOG_SET_LEVEL('INFO', module=module)

    if args.version:
        INFO(f'lustro {lustro.__version__}')
        INFO(f'python {sys.version}')
        INFO(f'PyMuPDF {fitz.version[0]} ({fitz.version[2]})')
        INFO(f'{platform.system()} {platform.platform()}')
        INFO(f'{distro.name(pretty=True)}')
        return 0

    if len(args.paths) < 1:
        parser.print_help()
        return -1

    if args.json:
        try:
            os.mkdir(args.json, 0o755)
        except FileExistsError:
            pass
        except FileNotFoundError:
            DECODE(f'Cannot create {args.json}')
            FATAL('Must be able to create directory specified with --json')

    if args.recursive:
        if args.raw:
            FATAL('Cannot use --raw with --recursive')
        scan = lustro.scan.Scan(args.paths, max_workers=int(args.workers),
                                debug=args.debug, json_dir=args.json,
                                content=args.content, ocr=args.ocr)
        scan.scan(progress=args.progress, documents_only=args.documents,
                  callback = lambda idx, msg_type, payload:
                  payload_logger(idx, msg_type, payload, full=args.full))
        return 0

    # Non-recursive version for debugging
    for path in args.paths:
        identify = lustro.identify.Identify(path, content=args.content,
                                            ocr=args.ocr, debug=args.raw)
        result = identify.identify()
        print(lustro.format.Format.pretty_print(result, full=args.full))

    return 0
