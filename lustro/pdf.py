#!/usr/bin/env python3
# pdf.py -*-python-*-

import json
import re
import time

import fitz
import PIL
import tesserocr


class PDF():
    def __init__(self, path, debug=False):
        self.path = path
        self.debug = debug

        fitz.TOOLS.mupdf_display_errors(False)
        fitz.TOOLS.store_shrink(100)
        try:
            self.document = fitz.Document(self.path)
        except:
            self.document = None

    @staticmethod
    def _clean(text):
        # FIXME: Stop using fitz and use something else?
        text = re.sub(r'\udddd', r'\\udddd', text)

        lines = []
        for line in text.split('\n'):
            line = line.strip()
            if len(line) == 0:
                continue
            lines.append(line)
        return ' '.join(lines)

    def _extract_text(self):
        result = []
        if self.document is None:
            return result
        for pageno, page in enumerate(self.document):
            blocks = page.get_text('blocks', flags=fitz.TEXT_INHIBIT_SPACES)
            line_in_work = 0
            for _, _, _, _, text, block_no, block_type in blocks:
                if block_type == 1:
                    # Skip image blocks
                    continue

                line_in_work += 1
                line = {}
                line['page'] = pageno
                line['line'] = block_no
                line['line_in_work'] = line_in_work
                line['text'] = self._clean(text)
                if len(line['text']) > 0:
                    result.append(line)
        return result

    def _ocr_text(self):
        result = []
        if self.document is None:
            return result
        total_time = 0
        start = time.time()
        for pageno, page in enumerate(self.document):
            pix = page.get_pixmap(matrix=fitz.Matrix(2, 2), alpha=False)
            img = PIL.Image.frombytes("RGB", [pix.width, pix.height],
                                      pix.samples)

            page_start = time.time()
            text = tesserocr.image_to_text(img)
            page_time = time.time() - page_start
            total_time = time.time() - start

            line = {}
            line['page'] = pageno
            line['text'] = self._clean(text)
            if len(line['text']) > 0:
                result.append(line)
        return result

    @staticmethod
    def add(result, field, data):
        if data is None or data == '':
            return
        if isinstance(data, str) and len(data) > 20:
            data = re.sub(' /.*', '', data)
        if field not in result:
            result[field] = data

    @staticmethod
    def read(path, content=False, ocr='yes', debug=False):
        result = {}
        pdf = PDF(path, debug=debug)
        if debug:
            print('PDF METADATA', json.dumps(pdf.document.metadata, indent=4,
                                             sort_keys=False), flush=True)
        if pdf.document is None:
            return result
        pdf.add(result, 'title', pdf.document.metadata.get('title', None))
        pdf.add(result, 'author', pdf.document.metadata.get('author', None))
        pdf.add(result, 'subject', pdf.document.metadata.get('subject', None))
        pdf.add(result, 'keywords', pdf.document.metadata.get('keywords',
                                                              None))

        if content:
            if ocr == 'force':
                cnt = pdf._ocr_text()
            else:
                try:
                    cnt = pdf._extract_text()
                except RuntimeError as exception:
                    result['error'] = f'Error on {path}: {repr(exception)}'
                    cnt = []
                if len(cnt) == 0 and ocr == 'yes':
                    content = pdf._ocr_text()
            if len(cnt) > 0:
                result['content'] = cnt
        return result
