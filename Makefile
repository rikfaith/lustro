# Makefile

.PHONY: lint

DLIST:=missing-function-docstring,missing-module-docstring
DLIST:=$(DLIST),missing-class-docstring,too-few-public-methods
DLIST:=$(DLIST),too-many-arguments,too-many-locals,too-many-instance-attributes
DLIST:=$(DLIST),too-many-branches,too-many-statements
DLIST:=$(DLIST),too-many-boolean-expressions,too-many-return-statements

lint:
	pep8 bin/* lustro/*.py
	pylint --disable=$(DLIST) \
		--init-hook="import sys; sys.path.append('.')" \
		--include-naming-hint=y \
		--good-names=fp,db \
		lustro

wheel:
	python3 setup.py sdist bdist_wheel

