# lustro

Illumination for files: determine file type, with details from metadata;
output JSON suitable for full text search.

## Dependencies

Dependencies are listed in requirements.debian as a list of Debian packages.

## Activate and Deactivate

Instead of sourcing scripts, I use two macros, as follows:

```bash
activate () {
        export VIRTUAL_ENV="$(pwd -P)" 
        _OLD_VIRTUAL_PATH="$PATH" 
        PATH="$VIRTUAL_ENV/bin:$PATH" 
        hash -r
        _OLD_VIRTUAL_PROMPT="$PROMPT" 
        PROMPT="%{%B%F{cyan}%}py:$(basename $VIRTUAL_ENV)%{%f%b%} $PROMPT" 
}
```

```bash
deactivate () {
        if [ -n "$_OLD_VIRTUAL_PATH" ]
        then
                PATH="$_OLD_VIRTUAL_PATH" 
                unset _OLD_VIRTUAL_PATH
        fi
        hash -r
        if [ -n "$_OLD_VIRTUAL_PROMPT" ]
        then
                PS1="$_OLD_VIRTUAL_PROMPT" 
                unset _OLD_VIRTUAL_PROMPT
        fi
        unset VIRTUAL_ENV
        unset PYTHONHOME
        unset PYTHONPATH
}
```

## Bugs filed

* https://bugs.ghostscript.com/show_bug.cgi?id=706093
* https://github.com/pymupdf/PyMuPDF/issues/2076
